const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const div = document.createElement("div");
div.setAttribute("id", "root");
document.body.append(div);
const ul = document.createElement("ul");
div.prepend(ul);
books.forEach(element => {
  try {
    if (!element.author) {
      throw new SyntaxError('Помилка! не вистачає "author"');
    } else if (!element.name) {
      throw new SyntaxError('Помилка! не вистачає "name"');
    } else if (!element.price) {
      throw new SyntaxError('Помилка! не вистачає "price"');
    }
    ul.insertAdjacentHTML(
      "beforeend",
      `<li> Автор: ${element.author} <br> Назва книги: ${element.name} <br> Ціна: ${element.price} </li>`
    );
  } catch (error) {
    console.log(error.message);
  }
});
